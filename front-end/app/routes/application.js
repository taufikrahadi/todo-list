import Route from '@ember/routing/route';
import { action, set } from "@ember/object";

export default class ApplicationRoute extends Route {
    async model() {
        return this.store.findAll('todo');
    }

    setupController(controller, model) {
        set(controller, 'todos', model);
    }
}
