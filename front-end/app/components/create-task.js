import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class CreateTaskComponent extends Component {
    @service('store') store;
    @tracked judul = '';
    @tracked deskripsi = '';
    @action
    submit() {
        let task = this.store.createRecord('todo', {
            judul : this.judul,
            deskripsi : this.deskripsi,
            status : false,
        });
        task.save().then(() => {
            alert('ditambahkan');
            this.judul = '';
            this.deskripsi = '';
        });
    }
}
