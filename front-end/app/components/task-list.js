import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { inject as service } from '@ember/service';

export default class TaskListComponent extends Component {
    @service('store') store;
    @tracked task = {};

    @action
    launchModal(task) {
        this.task = task;
    }
    @action
    deleteData(id) {
        let task = this.store.peekRecord('todo', id);
        task.deleteRecord();
        task.isDeleted;
        task.save();
        alert('Deleted');
    }
    @action
    completeTask(id) {
        this.store.findRecord('todo', id).then(task => {
            task.status = true;
            task.save();
            alert('mantep')
        });
    }

    @action
    editModal(task) {
        this.task = task;
    }
    @action
    editData(id) {
        this.store.findRecord('todo', id).then(task => {
            task.judul = this.task.judul;
            task.deskripsi = this.task.deskripsi;
            task.save();
            alert('updated');
            this.task = {};
        })
    }
}
