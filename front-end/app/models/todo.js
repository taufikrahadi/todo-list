import Model, { attr } from '@ember-data/model';

export default class TodoModel extends Model {
    @attr('string') judul;
    @attr('string') deskripsi;
    @attr('boolean') status;
}