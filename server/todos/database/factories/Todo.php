<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Todo;
use Faker\Generator as Faker;

$factory->define(Todo::class, function (Faker $faker) {
    return [
        'judul' => 'aq makan siang',
        'deskripsi' => 'makan siang di warteg',
        'status' => '1'
    ];
});
