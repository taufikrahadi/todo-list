<?php

namespace Tests\Feature;


use App\Todo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Faker;

use function PHPSTORM_META\type;

class TodoTest extends TestCase
{
    /**
     * @var string
     */
    protected $resourceType = 'todos';

    use DatabaseMigrations;
    public function testReadJsonApiWIthId(){

        $model = factory(Todo::class)->create();

        $self = "http://localhost:8000/api/v1/todos/{$model->getKey()}";

        $response = $this->get($self);
        $response->assertStatus(200);

    }

    public function testCanDelete(){

        // arrange
        $model = factory(Todo::class)->create();

        // act and assert
        $this->delete(route('api:v1:todos.delete', $model->id))
        ->assertStatus(204);
    }

    public function testRead()
    {
        $model = factory(Todo::class)->create();

        $self = "http://localhost:8000/api/v1/todos/{$model->getKey()}";

        $data = [
            'type' => 'todos',
            'id' => (string) $model->getRouteKey(),
            'attributes' => [
                'judul' => $model->judul,
                'deskripsi' => $model->deskripsi,
                'status' => $model->status,
            ],

        ];
        $this->doRead($model)->assertRead($data);
    }

    public function testCreate()
    {
        $post = factory(Todo::class)->make();

        $data = [
            'type' => 'todos',
            'attributes' => [
                'judul' => $post->judul,
                'deskripsi' => $post->deskripsi,
                'status' => $post->status,
            ],
        ];
        $id = $this->doCreate($data)->assertCreatedWithId($data);
        $id = $this
            ->doCreate($data)
            ->assertCreatedWithId($data);
    }

    public function testUpdate()
    {
        $todo = factory(Todo::class)->create();

        $data = [
            'type' => 'todos',
            'id' => (string) $todo->getRouteKey(),
            'attributes' => [
                'judul' => 'mission completed',
            ],
        ];
        $this->doUpdate($data)->assertUpdated($data);
    }

}
