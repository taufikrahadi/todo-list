<?php

namespace App\JsonApi\Todos;

use App\Todo;
use CloudCreativity\LaravelJsonApi\Document\ResourceObject;
use CloudCreativity\LaravelJsonApi\Eloquent\AbstractAdapter;
use CloudCreativity\LaravelJsonApi\Pagination\StandardStrategy;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Neomerx\JsonApi\Contracts\Encoder\Parameters\EncodingParametersInterface;

class Adapter extends AbstractAdapter
{

    /**
     * Mapping of JSON API attribute field names to model keys.
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * Mapping of JSON API filter names to model scopes.
     *
     * @var array
     */
    protected $filterScopes = [];

    /**
     * Adapter constructor.
     *
     * @param StandardStrategy $paging
     */
    public function __construct(StandardStrategy $paging)
    {
        parent::__construct(new \App\Todo(), $paging);
    }

    /**
     * @param Builder $query
     * @param Collection $filters
     * @return void
     */
    protected function filter($query, Collection $filters)
    {
        $this->filterWithScopes($query, $filters);
    }
    /**
     * @inheritDoc
     */
    protected function createRecord(ResourceObject $resource)
    {
      return new Todo ([
            'judul'=>$resource->judul,
            'deskripsi'=>$resource->deskripsi,
            'status'=>$resource->status,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function update($record, array $document, EncodingParametersInterface $parameters)
    {
        $parameters = $this->getQueryParameters($parameters);

        /** @var Model $record */
        $record = parent::update($record, $document, $parameters);
        $this->load($record, $parameters);
        return $record;
    }
    /**
     * @param Site $record
     * @return object
     */
    protected function persist($record)
    {
        $record->save();

        return $record;
    }
    /**
     * @param Site $record
     * @return bool
     */
    public function destroy($record)
    {
        $record->delete();

        return true;
    }


}
